import { ImapPage } from './app.po';

describe('imap App', () => {
  let page: ImapPage;

  beforeEach(() => {
    page = new ImapPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
