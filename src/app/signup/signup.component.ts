import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { SimpleGlobal } from 'ng2-simple-global';
import { UserService } from '../_services/user.service';

import { SharedService } from '../_services/shared.service';

@Component({
    moduleId: module.id,
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    model: any = {};
    loginModel: any = {};
    countUsers: number = 0;
    loading = false;
    error = '';

    constructor(
        private router: Router,
        private gb: SimpleGlobal,
        private sharedService: SharedService,
        private authenticationService: AuthenticationService,
        private userService: UserService) { }

    ngOnInit() {
        this.authenticationService.logout(true);
        this.gb['user'] = { };

        this.userService.getCountUsers().subscribe( number => {
            this.countUsers = number;
        });
    }

    signup() {
        this.loading = true;

        this.authenticationService.signup(this.model).subscribe(
            response => {
                this.authenticationService.login(this.model.email, this.model.password)
                .subscribe(result => {
                    this.userService.getCurrent().subscribe(user => this.gb['user'] = user);
                    this.sharedService.initUser();
                    this.router.navigate(['/']);
                });
            },
            error => {
                this.error = 'Username or password is incorrect';
                this.loading = false;
            }
        );
    }

    login(): void{
        this.loading = true;
        this.authenticationService.login(this.loginModel.email, this.loginModel.password)
        .subscribe(result => {
            if (result === true) {
                this.userService.getCurrent().subscribe(user => this.gb['user'] = user);
                this.sharedService.initUser();
                this.router.navigate(['/']);
            } else {
                this.error = 'Username or password is incorrect';
            }
            this.loading = false;
        });
    }
}
