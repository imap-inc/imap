import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes } from 'ngx-uploader';

import { SharedService } from '../_services/shared.service';
import { ApiConfig } from '../api.config';
import { UserService } from '../_services/user.service';
import { DialogService } from '../_services/dialog.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit{
    private sub: any;
    private myProfile: boolean = true;

    uploadInput: EventEmitter<UploadInput>;
    humanizeBytes: Function;
    dragOver: boolean;

    user: any;

    public points: any = [];


    constructor(
        private route: ActivatedRoute,
        private gb: SimpleGlobal,
        private sharedService: SharedService,
        private userService: UserService,
        private api: ApiConfig,
        private router: Router,
        private dialogService: DialogService) {
        this.uploadInput = new EventEmitter<UploadInput>();
        this.humanizeBytes = humanizeBytes;

        this.user = {
            coords: {},
            following: [],
            followers: [],
            points: []
        };
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let interval = setInterval(() => {
                if(this.gb['loading'] == false){

                    this.userService.getById(params['id']).subscribe(user => {
                        if(this.gb['user']._id != params['id']){
                            this.myProfile = false;
                            this.sharedService.publishMarker(user);
                        } else {
                            this.myProfile = true;
                            this.gb['user'] = user;
                        }

                        this.user = user;
                        this.sharedService.publishCenterMap(user.coords.lat, user.coords.lng);

                    }, err => {
                        this.router.navigate([this.gb['user']._id]);
                    });
                    clearInterval(interval);
                }
            }, 50);
        });

        this.sharedService.point.subscribe(data => {
            this.user.points.unshift(data);
        });
    }

    isMyProfile(): boolean{
        return this.myProfile;
    }

    isFollow(): boolean{
        return this.gb['user'].following.indexOf(this.user._id) != -1;
    }

    isOnline(): string{
        return this.isMyProfile() ? 'online' : this.user.isOnline ? 'online' : '';
    }

    getAvatarUrl(){
        if(this.user.avatar){
            return this.api.url(`/${this.user._id}/avatar.jpg`);
        } else {
            return this.api.url('/no-avatar.png');
        }
    }



    createDialog(): void{
        if(!this.myProfile){
            this.dialogService.createDialog(this.user._id).subscribe(dialog => {
                this.router.navigate([`/dialog/${dialog._id}`]);
            });
        }
    }



    follow(){
        if(!this.myProfile){
            this.userService.follow(this.user._id).subscribe(data => {
                if(this.gb['user'].following.indexOf(data.userId) == -1){
                    this.gb['user'].following.push(data.userId);
                }
            });
        }
    }



    unfollow(){
        if(!this.myProfile){
            this.userService.unfollow(this.user._id).subscribe(data => {
                let index = this.gb['user'].following.indexOf(data.userId);
                if (index != -1) {
                    this.gb['user'].following.splice(index, 1);
                }
            });
        }
    }



    onUploadOutput(output: UploadOutput): void {
        if(this.myProfile){
            if(output.type == 'done'){
                window.location.href = `/${this.gb['user']._id}`;
            }

            if (output.type === 'allAddedToQueue') { // when all files added in queue
                const event: UploadInput = {
                    type: 'uploadAll',
                    url: this.api.url('/api/user/upload-avatar'),
                    method: 'POST',
                    headers: {'x-access-token':this.api.getToken() },
                    concurrency: 1
                };
                this.uploadInput.emit(event);
            } else if (output.type === 'dragOver') { // drag over event
                this.dragOver = true;
            } else if (output.type === 'dragOut') { // drag out event
                this.dragOver = false;
            } else if (output.type === 'drop') { // on drop event
                this.dragOver = false;
            }
        }
    }



    startUpload(): void {
        if(this.myProfile){
            const event: UploadInput = {
                type: 'uploadAll',
                url: this.api.url('/api/user/upload-avatar'),
                method: 'POST',
                headers: {'x-access-token':this.api.getToken() },
                concurrency: 1
            };

            this.uploadInput.emit(event);
        }
    }



    cancelUpload(id: string): void {
        this.uploadInput.emit({ type: 'cancel', id: id });
    }

}
