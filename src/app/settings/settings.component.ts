import { Component, OnInit } from '@angular/core';
import { SimpleGlobal } from 'ng2-simple-global';

import { ApiConfig } from '../api.config';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    constructor(
        private gb: SimpleGlobal,
        private api: ApiConfig,
    ) { }

    ngOnInit() {
    }

    getAvatarUrl(){
        if(this.gb['user'].avatar){
            return this.api.url(`/${this.gb['user']._id}/avatar.jpg`);
        } else {
            return this.api.url('/no-avatar.png');
        }
    }
}
