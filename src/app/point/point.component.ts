import { Component, OnInit, Input } from '@angular/core';

import { ApiConfig } from '../api.config';
import { SimpleGlobal } from 'ng2-simple-global';
import { PointService } from '../_services/point.service';

@Component({
	selector: 'app-point',
	templateUrl: './point.component.html',
	styleUrls: ['./point.component.scss']
})
export class PointComponent implements OnInit {
	@Input() point: any;

	constructor(
        private api: ApiConfig,
        private gb: SimpleGlobal,
        private pointService: PointService,
    ) { }

	ngOnInit() {
	}

	getImg(): string{
	    return this.api.url(`${this.point.userId}/points/600/${this.point.fileName}`);
    }

    getLimUserLikes(){
	    return this.point.likes.slice(0, 10);
    }

    getOriginImg(): string{
        return this.api.url(`${this.point.userId}/points/original/${this.point.fileName}`);
    }

    getUserAvatar(userId: string = this.point.userId): string{
	    return this.api.url(`${userId}/avatar.jpg`);
    }

    toggleLike(): void{
        this.pointService.toggleLike(this.point._id).subscribe(data => {
            this.point = data;
        });
    }

    isSettedLike(): boolean{
	    return this.point.likes.indexOf(this.gb['user']._id) != -1;
    }

}
