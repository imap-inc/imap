import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { WebSocketService } from 'angular2-websocket-service'
import { SimpleGlobal } from 'ng2-simple-global';
import { NgUploaderModule } from 'ngx-uploader';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MasonryModule } from 'angular2-masonry';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';
import { ClickOutsideModule } from 'ng-click-outside';
import { ResponsiveModule, ResponsiveConfig } from 'ng2-responsive'
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import 'hammerjs';

import { AppComponent } from './app.component';
import { ApiConfig } from './api.config';


import { AuthGuard } from './_guards/auth.guard';
import { SignupComponent } from './signup/signup.component';



import { SharedService } from './_services/shared.service';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';
import { PointService } from './_services/point.service';
import { DialogService } from './_services/dialog.service';
import { MessageService } from './_services/message.service';



import { DialogComponent } from './dialog/dialog.component';
import { NonSelectedDialogComponent } from './dialog/non-selected-dialog/non-selected-dialog.component';

import { MessageComponent } from './dialog/message/message.component';

import { ProfileComponent } from './profile/profile.component';

import { FriendsComponent } from './friends/friends.component';
import { FriendsAllComponent } from './friends/all/friends-all.component';
import { FriendsOnlineComponent } from './friends/online/friends-online.component';
import { FriendsSearchComponent } from './friends/search/friends-search.component';

import { AddPointComponent } from './add-point/add-point.component';
import { PointComponent } from './point/point.component';

import { NewsComponent } from './news/news.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { MapComponent } from './map/map.component';

import { SettingsComponent } from './settings/settings.component';

import { GoogleMapContextMenuComponent } from './map/google-map-context-menu/google-map-context-menu.component';
import { GoogleMapInfoWindowComponent } from './map/google-map-info-window/google-map-info-window.component';



let config = {
    breakPoints: {
        xs: {max: 0},
        sm: {min: 0, max: 960},
        md: {min: 0, max: 0},
        lg: {min: 0, max: 0},
        xl: {min: 961},
    },
    debounceTime: 100 // allow to debounce checking timer
};

export function ResponsiveDefinition(){
    return new ResponsiveConfig(config);
}

export class MyHammerConfig extends HammerGestureConfig  {
    overrides = <any>{
        'swipe': {velocity: 0.4, threshold: 20} // override default settings
    }
}


const ROUTES = [
    {path: 'signup', component: SignupComponent},
    {path: '', redirectTo: 'dialog', pathMatch: 'full'},
    {path: 'news', component: NewsComponent, canActivate: [AuthGuard]},
    {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]},
    {path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard]},
    {path: 'friends', component: FriendsComponent, canActivate: [AuthGuard],
        children: [
            { path: 'all', component:  FriendsAllComponent, canActivate: [AuthGuard]},
            { path: 'online', component: FriendsOnlineComponent, canActivate: [AuthGuard]},
            { path: 'search', component: FriendsSearchComponent, canActivate: [AuthGuard]},
        ]
    },
    {path: 'dialog', component: DialogComponent, canActivate: [AuthGuard],
        children: [
            { path: '', component:  NonSelectedDialogComponent, canActivate: [AuthGuard]},
            { path: ':id', component: MessageComponent, canActivate: [AuthGuard]},
        ]
    },
    {path: ':id', component: ProfileComponent, canActivate: [AuthGuard]}
];



@NgModule({
    declarations: [
        AppComponent,
        MessageComponent,
        ProfileComponent,
        DialogComponent,
        SignupComponent,
        NonSelectedDialogComponent,
        FriendsComponent,
        FriendsAllComponent,
        FriendsOnlineComponent,
        FriendsSearchComponent,
        AddPointComponent,
        PointComponent,
        NewsComponent,
        NotificationsComponent,
        MapComponent,
        SettingsComponent,
        GoogleMapContextMenuComponent,
        GoogleMapInfoWindowComponent
    ],
    imports: [
        ClickOutsideModule,
        BrowserModule,
        NgUploaderModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(ROUTES),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDICKYHmn7p-LH-VDYC4fRguZylKdXy5XQ'
        }),
        MasonryModule,
        ResponsiveModule
    ],
    providers: [
        GoogleMapsAPIWrapper,
        SimpleGlobal,
        SharedService,
        ApiConfig,
        AuthGuard,
        AuthenticationService,
        MessageService,
        DialogService,
        UserService,
        PointService,
        WebSocketService,
        {provide: ResponsiveConfig,
         useFactory: ResponsiveDefinition},
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: MyHammerConfig
        } ],
    bootstrap: [AppComponent]
})
export class AppModule { }

