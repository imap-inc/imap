import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiConfig {
    public port: number = 443; // 443 for production

    private baseUrl: string  = '';


    constructor() {}

    url(url: string = ''): string{
        return `${this.baseUrl}${url}`;
    }

    getTokenParams(): string{
        return '_token=' + localStorage.getItem('token');
    }



    getToken(): string{
        return localStorage.getItem('token');
    }



    getReqOption() {
        let headersOption = {
            'Content-Type': 'application/json; charset=utf-8',
        };

        let token = localStorage.getItem('token');

        if(token){
            headersOption['x-access-token'] = token;
        }

        let headers = new Headers(headersOption);

        return new RequestOptions({ headers });
    }



    handleError(err: any){
        return Observable.throw(err.message || err);
    }
}
