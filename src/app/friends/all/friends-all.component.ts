import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiConfig } from '../../api.config';
import { SharedService } from '../../_services/shared.service';
import { UserService } from '../../_services/user.service';
import { DialogService } from '../../_services/dialog.service';

@Component({
	selector: 'app-friends-all',
	templateUrl: './friends-all.component.html',
	styleUrls: ['./friends-all.component.scss']
})
export class FriendsAllComponent implements OnInit {
    private timeoutSearch;

    users: any = [];

	constructor(
		private router: Router,
        private api: ApiConfig,
		private sharedService: SharedService,
		private userService: UserService,
		private dialogService: DialogService) { }

	ngOnInit() {
		this.search('');
	}

	getImg(item: any){
		return this.api.url(item.avatar ? `/${item._id}/avatar-80.jpg`: '/no-avatar.png');
	}

	createDialog(id): void{
		this.dialogService.createDialog(id).subscribe(dialog => {
			this.router.navigate([`/dialog/${dialog._id}`]);
		});
	}

	showInMap(item: any){
		this.sharedService.publishCenterMap(item.coords.lat, item.coords.lng);
	}

	search(filter){
        this.userService.getAllFollowing(filter).subscribe(users =>{
            this.users = users;
            this.sharedService.publishMarkers(users);
        });
    }

    runSearch(filter){
        if(this.timeoutSearch) clearTimeout(this.timeoutSearch);

        this.timeoutSearch = setTimeout(() => {
            this.search(filter);
        }, 350);
    }

    follow(item: any){
        this.userService.follow(item._id).subscribe(data => {
            this.users.find((item, index) => {
                if(item._id == data.userId){
                    this.users[index].unfollow = false;
                }
            });
        });
    }

    unfollow(item: any) {
        this.userService.unfollow(item._id).subscribe(data => {
            this.users.find((item, index) => {
                if (item._id == data.userId) {
                    this.users[index].unfollow = true;
                }
            });
        });
    }

    nextPage(){ this.router.navigate(['/friends/online']) };
}
