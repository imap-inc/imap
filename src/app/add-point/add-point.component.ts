import { Component, OnInit, ElementRef, EventEmitter, ViewChild } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes } from 'ngx-uploader';
import { SimpleGlobal } from 'ng2-simple-global';

import { ApiConfig } from '../api.config';
import { SharedService } from '../_services/shared.service';

@Component({
    selector: 'app-add-point',
    templateUrl: './add-point.component.html',
    styleUrls: ['./add-point.component.scss']
})
export class AddPointComponent implements OnInit {
    @ViewChild('textarea') private textarea: ElementRef;
    @ViewChild('imgFiles') private imgFiles: ElementRef;

    public img: any;
    public textareaModel: any;
    public uploadInput: EventEmitter<UploadInput>;
    public humanizeBytes: Function;

    constructor(
        private gb: SimpleGlobal,
        private api: ApiConfig,
        private sharedService: SharedService) {
        this.uploadInput = new EventEmitter<UploadInput>();
        this.humanizeBytes = humanizeBytes;
    }

    ngOnInit() {
    }



    resize(): void{
        let numberOfLineBreaks = (this.textarea.nativeElement.value.match(/\n/g)||[]).length;
        if( numberOfLineBreaks < 5) this.textarea.nativeElement.setAttribute('rows', numberOfLineBreaks + 1);
    }



    removeImg(){
        this.img = undefined;
    }


    onUploadOutput(output: UploadOutput): void {
        if(output.type === 'done'){
            this.clear();
            this.sharedService.publishPoint(output.file.response);
        } else if(output.type === "addedToQueue"){
            let reader = new FileReader();
            reader.onloadend = (e) => {
                this.img = e.srcElement;
            };
            reader.readAsDataURL(this.imgFiles.nativeElement.files[output.file.fileIndex]);
        }
    }



    publish(): void{
        const event: UploadInput = {
            type: 'uploadAll',
            url: this.api.url('/api/point/add'),
            method: 'POST',
            data: {
                lat: this.gb['user'].coords.lat,
                lng: this.gb['user'].coords.lng,
                text: this.textarea.nativeElement.value
            },
            headers: {'x-access-token': this.api.getToken() },
            concurrency: 1
        };
        this.uploadInput.emit(event);
    }

    clear(): void{
        this.textarea.nativeElement.value = "";
        this.img = undefined;
    }

}
