import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-google-map-context-menu',
    templateUrl: './google-map-context-menu.component.html',
    styleUrls: ['./google-map-context-menu.component.scss']
})
export class GoogleMapContextMenuComponent{
    @Output() sendCoords = new EventEmitter();

    onSendCoords() {
        this.sendCoords.emit();
    }
}
