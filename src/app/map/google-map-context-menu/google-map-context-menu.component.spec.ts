import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapContextMenuComponent } from './google-map-context-menu.component';

describe('GoogleMapContextMenuComponent', () => {
  let component: GoogleMapContextMenuComponent;
  let fixture: ComponentFixture<GoogleMapContextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapContextMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
