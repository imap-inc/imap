import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';

import { ApiConfig } from '../api.config';
import { SharedService } from '../_services/shared.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
    private timeOutContextMenuOpen;
    private timeOutInfoWindow;
    private contextMenuCoords: string = "";
    public mapData: any = {
		zoom: 16,
		styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"administrative","elementType":"labels","stylers":[{"saturation":"-100"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"gamma":"0.75"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"lightness":"-37"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f9f9f9"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"40"},{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"labels.text.fill","stylers":[{"saturation":"-100"},{"lightness":"-37"}]},{"featureType":"landscape.natural","elementType":"labels.text.stroke","stylers":[{"saturation":"-100"},{"lightness":"100"},{"weight":"2"}]},{"featureType":"landscape.natural","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"80"}]},{"featureType":"poi","elementType":"labels","stylers":[{"saturation":"-100"},{"lightness":"0"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"lightness":"-4"},{"saturation":"-100"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"},{"visibility":"on"},{"saturation":"-95"},{"lightness":"62"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road","elementType":"labels","stylers":[{"saturation":"-100"},{"gamma":"1.00"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"gamma":"0.50"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"gamma":"0.50"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"},{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"-13"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"lightness":"0"},{"gamma":"1.09"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"},{"saturation":"-100"},{"lightness":"47"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"lightness":"-12"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"},{"lightness":"77"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"-5"},{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"saturation":"-100"},{"lightness":"-15"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"lightness":"47"},{"saturation":"-100"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"water","elementType":"geometry","stylers":[{"saturation":"53"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"-42"},{"saturation":"17"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":"61"}]}],
		markers: [],
		centerLat: 0,
		centerLng: 0,
	};
    public contextMenuIsOpen: boolean = false;
    public contextMenuPosition: any = {'left': '30px', 'top': 0};



  	constructor(
        private router: Router,
        private api: ApiConfig,
		private sharedService: SharedService,
        public gb: SimpleGlobal) {
    }

  	ngOnInit() {

        this.sharedService.coords.subscribe( data => {
            this.mapData.markers = [];
            this.mapData.markers.push({
                lat: +data.lat,
                lng: +data.lng,
                fullName: '',
                opacity: 1,
            });
        });

  		this.sharedService.marker.subscribe( data => {
			this.mapData.markers = [];
			this.mapData.markers.push({
				lat: +data.coords.lat.toFixed(7),
				lng: +data.coords.lng.toFixed(7),
                fullName: `${data.firstName} ${data.lastName}`,
                iconUrl: this.api.url(data.avatar ? `${data._id}/marker.png` : `/no-marker.png`),
                opacity: data.isOnline ? 1 : 0.5
			});
		});

        this.sharedService.markers.subscribe( data => {
            this.mapData.markers = [];
            Array.prototype.forEach.call(data, (item) => {
                this.mapData.markers.push({
                    lat: +item.coords.lat.toFixed(7),
                    lng: +item.coords.lng.toFixed(7),
                    fullName: `${item.firstName} ${item.lastName}`,
                    iconUrl: this.api.url(item.avatar ? `${item._id}/marker.png` : `/no-marker.png`),
                    opacity: item.isOnline ? 1 : 0.6
                });
            });

        });

		this.sharedService.centerMap.subscribe( data => {
		    this.mapData.centerLat = +data.lat;
			this.mapData.centerLng = +data.lng;
		});
  	}


    @HostListener('mouseup', ['$event']) onClick(e) {
  	    if(e.button === 2){
  	        this.contextMenuPosition = {
                'left': e.clientX + 'px',
                'top': e.clientY - 40 + 'px'
            }
        }
    }


    sendCoords(): void{
        this.sharedService.sendCoords(this.contextMenuCoords);
        this.contextMenuIsOpen = false;
    }



    showInfo(info, event): void{
  	    let data = {
  	        'fullName': info.fullName,
            'id': info.iconUrl.split('/')[0],
        };
        this.sharedService.publishMapInfoWindow(data);

        if(this.timeOutInfoWindow) clearTimeout(this.timeOutInfoWindow);
        this.timeOutInfoWindow = setTimeout(() => {
            this.sharedService.publishMapInfoWindow('close');
        }, 5000);
    }


    getUserMarker(): string{
  	    return this.api.url(this.gb['user'].avatar ? `${this.gb['user']._id}/marker.png` : `/no-marker.png`);
    }



    openMenu(event): void{
        if(new RegExp('^/dialog/').test(this.router.url)) {
            let lat = event.coords.lat.toFixed(7);
            let lng = event.coords.lng.toFixed(7);
            this.contextMenuCoords = lat + ' ' + lng;

            this.contextMenuIsOpen = true;

            this.sharedService.publishCoords(lat, lng);

            if(this.timeOutContextMenuOpen) clearTimeout(this.timeOutContextMenuOpen);

            this.timeOutContextMenuOpen = setTimeout(() => {
                this.contextMenuIsOpen = false;
            }, 3000);
        }
    }


}
