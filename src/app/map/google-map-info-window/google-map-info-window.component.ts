import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../_services/shared.service';

@Component({
    selector: 'app-google-map-info-window',
    templateUrl: './google-map-info-window.component.html',
    styleUrls: ['./google-map-info-window.component.scss']
})
export class GoogleMapInfoWindowComponent implements OnInit {
    private info: any;

    constructor(private sharedService: SharedService) {
        this.info = {
            'opening': false,
        }
    }

    ngOnInit() {
        this.sharedService.mapInfoWindow.subscribe( data => {
            if(data == 'close'){
                this.info.opening = false;
            } else {
                this.info.opening = true;
                this.info.data = data;
            }
        });
    }



    getHeight(): string {
        if (!this.info.opening) return '0px';
        let height = '200px';
        return this.info.opening ? height : '0px';
    }


    getName(): string{
        return this.info.data ? this.info.data.fullName : '';
    }



    viewProfile(): void{

    }
}
