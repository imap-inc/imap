import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapInfoWindowComponent } from './google-map-info-window.component';

describe('GoogleMapInfoWindowComponent', () => {
  let component: GoogleMapInfoWindowComponent;
  let fixture: ComponentFixture<GoogleMapInfoWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapInfoWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapInfoWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
