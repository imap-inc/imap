import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SimpleGlobal } from 'ng2-simple-global';

import { ApiConfig } from '../api.config';
import { DialogService } from '../_services/dialog.service';
import { SharedService } from '../_services/shared.service';

@Component({
	selector: 'app-dialog',
	templateUrl: './dialog.component.html',
	styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
	public dialogs: any = [];
    public dialogIsOpen: boolean = false;

	constructor(
        private router: Router,
		private gb: SimpleGlobal,
        private api: ApiConfig,
		private sharedService: SharedService,
		private dialogService: DialogService) { }

	ngOnInit() {

        this.router.events.subscribe(() => {
            this.dialogIsOpen = this.router.url != '/dialog';
        });

		this.dialogService.getAll().subscribe(dialogs => {
			this.dialogs = dialogs;
			this.dialogs.sort((a, b) => { return a.date < b.date; });
		});

		this.sharedService.dialog.subscribe( dialog => {
			let isFind = false;

			this.dialogs.find((item, index) => {
				if(item._id == dialog._id){
					isFind = true;
					this.dialogs[index] = dialog;
					this.dialogs = this.dialogs.sort((a, b) => { return a.date < b.date; });
				}
			});

			if(!isFind){
				this.dialogs.push(dialog);
			}
		});
	}



	getName(item: any): string{
		return item.twoMembers ? item.twoMembers[this.gb['user']._id] : item.name;
	}

	getImg(item: any): string{
		let id = Object.keys(item.twoMembers)[0];
		if(id == this.gb['user']._id){
			id = Object.keys(item.twoMembers)[1];
		}
		return this.api.url(`/${id}/avatar-30.jpg`);
	}

	getLastSenderImg(): string{
	    return this.api.url(`/${this.gb['user']._id}/avatar-30.jpg`);
    }

	getUnreadMsg(item: any): number{
		let id = Object.keys(item.twoMembers)[0];
		if(id == this.gb['user']._id){
			id = Object.keys(item.twoMembers)[1];
		}

		let curentUser = item.users.find((user, index) => {
			return user._id == this.gb['user']._id;
		});

		return curentUser.unreadMsg;
	}

	getLastMessage(item: any): any{
	    if(!item.lastMessage) return '';
	    else if(item.lastMessage.type == 'msg') return item.lastMessage.content.replace(/\\n/g , " ");
        else if(item.lastMessage.type == 'img') return "Photo";
        else if(item.lastMessage.type == 'coords') return "Coordinates";
        else return "";
	}



  	createDialog(userId: string): void{
		this.dialogService.createDialog(userId).subscribe(dialog => this.dialogs.push(dialog));
	}



    setFocus(): void{
	    
    }
}
