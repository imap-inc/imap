import { Component, OnInit, AfterViewChecked, ElementRef, EventEmitter, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription'
import { SimpleGlobal } from 'ng2-simple-global';
import { UploadOutput, UploadInput } from 'ngx-uploader';

import { DialogService } from '../../_services/dialog.service';
import { MessageService } from '../../_services/message.service';
import { UserService } from '../../_services/user.service';
import { SharedService } from '../../_services/shared.service';
import { ApiConfig } from '../../api.config';

@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, AfterViewChecked{
    @ViewChild('scroll') private myScrollContainer: ElementRef;
    @ViewChild('img') private fileImg: ElementRef;
	@ViewChild('message') private message: ElementRef;

	private socketSubscription: Subscription;
	private sendingCoords: Subscription;
	private sub: any;
 	private loadMessageCount: number = 50;

    uploadInput: EventEmitter<UploadInput>;


	public switcher: number = 1;

	startHeight: number = 0;
	id: string;
	dialog: any = {};
	theEnd: boolean = false;



	constructor(
        private route: ActivatedRoute,
        private router: Router,
		private gb: SimpleGlobal,
		private sharedService: SharedService,
        private api: ApiConfig,
		private dialogService: DialogService,
        private userService: UserService,
		private socket: MessageService) {
        this.uploadInput = new EventEmitter<UploadInput>();
    }



	ngOnInit(){

        this.sub = this.route.params.subscribe(params => {
       		this.id = params['id'];

       		if(this.socketSubscription){
       			this.socketSubscription.unsubscribe();
       		}

       		this.dialogService.getOneById(this.id).subscribe(
       			dialog => {
       				this.dialog = dialog;
       				setTimeout(() => {
						this.scrollToBottom();
					}, 10);
                    let ids = Array.from(this.dialog.users, x => x['_id']);
                    console.log(ids);
                    this.userService.filterById(ids).subscribe(users =>{
                        this.sharedService.publishMarkers(users);
                    });
       			},
       			err => this.router.navigateByUrl('dialog')
  			);

			let stream = this.socket.connect(this.id);
			this.socketSubscription = stream.subscribe(message => {
				if(!this.dialog.messages) this.dialog.messages = [];
				this.dialog.messages.push(message);
				this.dialog.lastMessage = message.msg;
				this.dialog.date = message.date;

				this.switcher = 1;
			});

            this.message.nativeElement.focus();
        });

        this.sendingCoords = this.sharedService.sendingCoords.subscribe( coords => {
            this.socket.send({'content': coords, 'type': 'coords'});
        });
	}

	ngOnDestroy() {
    	this.sub.unsubscribe();
		this.socketSubscription.unsubscribe();
		this.sendingCoords.unsubscribe();
  	}

	ngAfterViewChecked() {
		if(this.switcher == 1){
			this.scrollToBottom();
			setTimeout(() => {
				this.switcher = 0;
			}, 50);
		}
    }


    onUploadOutput(output: UploadOutput): void {
        if (output.type === 'allAddedToQueue') {
            const event: UploadInput = {
                type: 'uploadAll',
                url: this.api.url(`/api/dialog/${this.id}/upload-img`),
                method: 'POST',
                headers: {'x-access-token': this.api.getToken() },
                concurrency: 1
            };
            this.uploadInput.emit(event);
        } else if(output.type == 'done'){
            this.socket.send({'content': output.file.response.filename, 'type': 'img'});
            this.fileImg.nativeElement.value = "";
        }
    }


    check_lat_lon(str: string): boolean{
        let ck_lat = new RegExp('^(\-)?([0-8][0-9]?|90)\.{1}([0-9]{1,7})$');
        let ck_lon = new RegExp('^(\-)?([0-1]?[0-7]?[0-9]|180)\.{1}([0-9]{1,7})$');
        let data = str.split(' ');

        if(data.length != 2) return false;

        let lat = data[0];
        let lon = data[1];

        return ck_lat.test(lat) && ck_lon.test(lon);
    }



    showInMap(item: any){
        let data = JSON.parse(item.msg.content).coords.split(' ');

	    this.sharedService.publishCoords(data[0], data[1]);
	    this.sharedService.publishCenterMap(data[0], data[1]);
    }



    getName(): string{
		return this.dialog.twoMembers ? this.dialog.twoMembers[this.gb['user']._id] : this.dialog.name;
	}

	getUserImg(item: any): string{
		return this.api.url('/' + item.userId + '/avatar-30.jpg');
	}

	getSender(item: any): string{
		return item.userId != this.gb['user']._id ? 'to-me' : '';
	}

    getFormattedAddress(item: any): string{
	    return JSON.parse(item.msg.content).formatted_address;
    }

    getCoords(item: any): string{
        return JSON.parse(item.msg.content).coords;
    }

    getCoordinates(item: any): string{
        return JSON.parse(item.msg.content).coords;
    }

	type(item: any, type: string): boolean{
	    return item.msg.type == type;
    }

	splitMessage(item: any): any{
		return item.msg.content.split("\n");
	}

    getImg(item: any): string{
	    return `/${this.id}/250/${item.msg.content}`;
    }



    loadOlderMessages(e): void{
    	if(this.switcher == 0 && e.target.scrollTop < 300 && !this.theEnd){
    		this.switcher = 2;
    		this.scrollFixStart();
			this.dialogService.getMessages(this.id, this.dialog.messages.length, this.loadMessageCount)
				.subscribe(data => {
					if(data['theEnd']) this.theEnd = true;
					else {
						for(let i in data){
							this.dialog.messages.unshift(data[i]);
							this.scrollFixEnd();
						}
	    				this.switcher = 0;
					}
				});
		}
	}

	sendMessage(st: HTMLInputElement, event: any): void{
		event.preventDefault();

		if(st.value != ''){
			let content = st.value;
			let type = 'msg';
			while(content.charAt(0) === '\n') {
                content = content.substr(1);
			}

			while(content.charAt(content.length-1) === '\n') {
                content = content.slice(1, -1);
			}

			if(this.check_lat_lon(content)) type = 'coords';

			this.socket.send({'content': content, 'type': type});
			st.value = null;
		}

        st.focus();
	}

	newLine(): void{
		let from = this.message.nativeElement.value.slice(0, this.message.nativeElement.selectionStart);
		let to = this.message.nativeElement.value.substr(this.message.nativeElement.selectionStart);

		let position = this.message.nativeElement.selectionStart + 1;

		this.message.nativeElement.value = from + "\n" + to;
		this.message.nativeElement.selectionStart = position;
		this.message.nativeElement.selectionEnd = position;
	}

	resize(): void{
		let numberOfLineBreaks = (this.message.nativeElement.value.match(/\n/g)||[]).length;
		if( numberOfLineBreaks < 5) this.message.nativeElement.setAttribute('rows', numberOfLineBreaks + 1);
	}



    scrollToBottom(): void {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }

    scrollFixStart(): void {
    	this.startHeight = this.myScrollContainer.nativeElement.scrollHeight;
    }

 	scrollFixEnd(): void {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight - this.startHeight + 270;
    }



	prevSender(i:number): boolean{
		if(i > 0){
			let prevDate = new Date(this.dialog.messages[i-1].date);
			let date = new Date(this.dialog.messages[i].date);
			prevDate.setMinutes(prevDate.getMinutes() + 2);

			return this.dialog.messages[i].userId == this.dialog.messages[i-1].userId && date < prevDate;
		}
		return false;
	}



	goBack(): void{
        window.history.back();
    }
}
