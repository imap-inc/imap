import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonSelectedDialogComponent } from './non-selected-dialog.component';

describe('NonSelectedDialogComponent', () => {
  let component: NonSelectedDialogComponent;
  let fixture: ComponentFixture<NonSelectedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonSelectedDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonSelectedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
