import { Component, OnInit } from '@angular/core';

import { ApiConfig } from '../api.config';
import { PointService } from '../_services/point.service';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
    public points: any = [];

    constructor(
        private apiConfig: ApiConfig,
        private pointService: PointService) {

        this.pointService.getAll().subscribe((data)=>{
             this.points = data;
        });
    }

    ngOnInit() {
    }

}
