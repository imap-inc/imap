import { Component, OnInit } from '@angular/core';
import { SimpleGlobal } from 'ng2-simple-global';
import { Subscription } from 'rxjs/Subscription'
import { Router } from '@angular/router';


import { SharedService } from './_services/shared.service';
import { ApiConfig } from './api.config';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
    private socketSubscription: Subscription;
    private centerMapChanging: boolean = true;
    public isOpen: boolean = false;
    public model: any = {};
    public loading = false;
    public error = '';
    public isOpenMap: boolean = false;



    constructor(
        private router: Router,
        private gb: SimpleGlobal,
        private sharedService: SharedService,
        private api: ApiConfig,
        private authenticationService: AuthenticationService,
        private userService: UserService) {
        this.gb['user'] = { 'loading': true, following: [], coords: {}, iconUrl: 'no-marker.png' };


        this.router.events.subscribe((data) => {
            this.isOpenMap = false;

            this.centerMapChanging = data['url'] == `/${this.gb['user']._id}`;
        });
    }



    ngOnInit() {
        this.sharedService.autorize.subscribe(() =>{
            this.initUser();
        });
        if(this.isAutorized()) this.initUser();
    }



    initUser(): void{
        if(this.socketSubscription){
            this.socketSubscription.unsubscribe();
        }

        this.userService.getCurrent().subscribe(user => {
            this.gb['user'] = user;

            this.setCoords(() => {

                this.gb['loading'] = false;

                let stream = this.userService.connect(user._id, this.gb['user'].coords.lat, this.gb['user'].coords.lng);
                this.socketSubscription = stream.subscribe(data => this.separator(data));

                this.sendCoords();
                this.sharedService.publishCenterMap(this.gb['user'].coords.lat, this.gb['user'].coords.lng);
            });
        });

        setInterval(()=>{
            this.setCoords((isNew) => {
               if(isNew){
                   this.sendCoords();

                   if(this.centerMapChanging){
                       this.sharedService.publishCenterMap(this.gb['user'].coords.lat, this.gb['user'].coords.lng);
                   }
               }
            });
        }, 2000);
    }



    ngOnDestroy() {
        this.gb['user'] = {};
    }



    getAvatarUrl(): string{
        if(this.gb['user'].avatar){
            return this.api.url(`/${this.gb['user']._id}/avatar-30.jpg`);
        } else {
            return this.api.url('/no-avatar.png');
        }
    }



    toggleOpenMenu(close): void{
        this.isOpen = !(close || this.isOpen);
    }



    checkOpen(): string{
        return this.isOpen ? 'open' : '';
    }




    isAutorized(): boolean{
        return this.authenticationService.isAutorized();
    }



    login(): void{
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
        .subscribe(result => {
            if (result === true) {
                this.userService.getCurrent().subscribe(user => this.gb['user'] = user);
                this.sharedService.initUser();
                this.router.navigate(['/']);
            } else {
                this.error = 'Username or password is incorrect';
            }
            this.loading = false;
        });
    }



    logout(): void{
        this.socketSubscription.unsubscribe();
        this.authenticationService.logout();
    }



    checkDiffCords(currCoord, newCoord){
        let diff = Math.abs(currCoord) - Math.abs(newCoord);
        let cor = 0.0000500;
        return  (diff > 0 && diff > cor) || (diff < 0 && diff < -cor);
    }

    setCoords(callback): void{
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(data => {
                if(!this.gb['user'].coords) this.gb['user'].coords = {};

                if(this.checkDiffCords(this.gb['user'].coords.lat, +data.coords.latitude.toFixed(7)) ||
                    this.checkDiffCords(this.gb['user'].coords.lng, +data.coords.longitude.toFixed(7))){

                    this.gb['user'].coords.lat = +data.coords.latitude.toFixed(7);
                    this.gb['user'].coords.lng = +data.coords.longitude.toFixed(7);

                    callback(true);
                } else {
                    callback(false);
                }
            });
        }
    }



    sendCoords(): void{
        let info = {
            type: 'coords',
            data: {
                lat: this.gb['user'].coords.lat,
                lng: this.gb['user'].coords.lng,
            }
        };

        this.userService.send(info);
    }


    separator(data): void{
        if(data.type == 'dialog'){
            this.sharedService.publishDialog(data.data);
        } else if (data.type == 'new-message'){

        }
    }



    toggleOpenMap(): void{
        this.isOpenMap = !this.isOpenMap;
    }

}
