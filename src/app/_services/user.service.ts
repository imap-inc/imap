import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { QueueingSubject } from 'queueing-subject'
import { Observable } from 'rxjs/Observable'
import { WebSocketService } from 'angular2-websocket-service'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { ApiConfig } from '../api.config';

@Injectable()
export class UserService {
    private inputStream: QueueingSubject<any>;
    public outputStream: Observable<any>;



    constructor(
        private http: Http,
        private api: ApiConfig,
        private socketFactory: WebSocketService) { }



    public connect(id: string, lat: string, lng: string) {
        let protocol = 'ws:';
        let port = `:${this.api.port}`;
        if(location.protocol == 'https:'){
            protocol = 'wss://';
        }
        return this.outputStream = this.socketFactory.connect(
            `${protocol}${location.hostname}${port}/ws/${id}/${this.api.getToken()}?lat=${lat}&lng=${lng}`,
            this.inputStream = new QueueingSubject<any>()
        );
    }



    public send(data: object) {
        this.inputStream.next(data);
    }


    getCountUsers(): Observable<any>{
        let url = this.api.url('/available-api/user/count');
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }


    getAll(): Observable<any>{
        let url = this.api.url('/api/user/all?') + this.api.getTokenParams();
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }



    getAllNotFollowing(filter: string = ''): Observable<any>{
        let url = this.api.url('/api/user/all-not-following?') + `name=${filter}&${this.api.getTokenParams()}`;
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }



    getAllFollowing(filter: string = ''): Observable<any>{
        let url = this.api.url('/api/user/all-following?') + `name=${filter}&${this.api.getTokenParams()}`;
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }



    getAllFollowingOnline(filter: string = ''): Observable<any>{
        let url = this.api.url('/api/user/all-following-online?') + `name=${filter}&${this.api.getTokenParams()}`;
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }



    getCurrent(): Observable<any>{
        let url = this.api.url('/api/user?') +  this.api.getTokenParams();
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }



    getById(id: string): Observable<any>{
        let url = this.api.url(`/api/user/${id}/?` + this.api.getTokenParams());
        return this.http.get(url).map(res => res.json()).catch(this.api.handleError);
    }



    filterById(arrayIds: any): Observable<any>{
        return this.http.post(this.api.url('/api/user/filter-by-id'), {'arrayIds': arrayIds}, this.api.getReqOption())
        .map(res => res.json())
        .catch(this.api.handleError);
    }



    follow(id: string): Observable<any>{
        return this.http.post(this.api.url('/api/user/follow'), {'id': id}, this.api.getReqOption())
        .map(res => res.json())
        .catch(this.api.handleError);
    }



    unfollow(id: string): Observable<any>{
        return this.http.post(this.api.url('/api/user/unfollow'), {'id': id}, this.api.getReqOption())
        .map(res => res.json())
        .catch(this.api.handleError);
    }
}
