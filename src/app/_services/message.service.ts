import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { QueueingSubject } from 'queueing-subject'
import { Observable } from 'rxjs/Observable'
import { WebSocketService } from 'angular2-websocket-service'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiConfig } from '../api.config';


@Injectable()
export class MessageService {
    private inputStream: QueueingSubject<any>;
    public outputStream: Observable<any>;

    constructor(private http: Http, private api: ApiConfig, private socketFactory: WebSocketService) { }

    public connect(dialogId: string) {
        let protocol = 'ws:';
        let port = `:${this.api.port}`;
        if(location.protocol == 'https:'){
            protocol = 'wss://';
        }
        return this.outputStream = this.socketFactory.connect(
            `${protocol}${location.hostname}${port}/ws/dialog/${dialogId}/${this.api.getToken()}`,
            this.inputStream = new QueueingSubject<any>()
        );
    }

    public send(msg: any):void {
        this.inputStream.next(msg);
    }


    getAll(dialogId: string): Observable<any[]>{
        let url = this.api.url('/api/message?') + this.api.getTokenParams() + '&id=' + dialogId;
        return this.http.get(url).map(res => res.json().messages);
    }
}
