import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiConfig } from '../api.config';

import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(
        private api: ApiConfig,
        private router: Router,
        private http: Http) {
        this.token = localStorage.getItem('token');
    }



    login(email: string, password: string): Observable<boolean> {
        let data = JSON.stringify({ email: email, password: password});
        let url = this.api.url('/auth/login');

        return this.http.post(url, data, this.api.getReqOption())
            .map((response: Response) => {
                let token = response.json() && response.json().token;
                if (token) {
                    this.token = token;
                    localStorage.setItem('token', token);
                    return true;
                } else {
                    localStorage.removeItem('token');
                    return false;
                }
            });
    }



    logout(noRedirect: boolean = false){
        let url = this.api.url('/auth/logout');

        this.http.post(url, null, null)
            .toPromise()
            .then(res => {
                this.token = null;
                localStorage.removeItem('token');
                if(!noRedirect) this.router.navigate(['/signup']);
            });
    }



    signup(data){
        let url = this.api.url('/auth/signup');

        return this.http.post(url, JSON.stringify(data), this.api.getReqOption())
            .map((response: Response) => {
                return response;
            });
    }



    isAutorized(){
        return this.token != null;
    }
}
