import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class SharedService {

    private $autorize = new Subject<any>();
    private $dialog = new Subject<any>();
    private $sendingCoords = new Subject<any>();
    private $coords = new Subject<any>();
    private $marker = new Subject<any>();
    private $markers = new Subject<any>();
    private $centerMap = new Subject<any>();
    private $mapInfoWindow = new Subject<any>();
    private $point = new Subject<any>();



    public autorize = this.$autorize.asObservable();
    public dialog = this.$dialog.asObservable();
    public sendingCoords = this.$sendingCoords.asObservable();
    public coords = this.$coords.asObservable();
    public marker = this.$marker.asObservable();
    public markers = this.$markers.asObservable();
    public centerMap = this.$centerMap.asObservable();
    public mapInfoWindow = this.$mapInfoWindow.asObservable();
    public point = this.$point.asObservable();



    initUser() {
        this.$autorize.next();
    }



    publishDialog(data: any) {
        this.$dialog.next(data);
    }



    publishCoords(lat: string, lng: string) {
        this.$coords.next({lat: lat, lng: lng});
    }

    sendCoords(coords: string) {
        this.$sendingCoords.next(coords);
    }

    publishMarker(data: any) {
        this.$marker.next(data);
    }

    publishMarkers(data: any) {
        this.$markers.next(data);
    }

    publishCenterMap(lat: any, lng: any) {
        this.$centerMap.next({lat: lat, lng: lng});
    }


    publishMapInfoWindow(data: any) {
        this.$mapInfoWindow.next(data);
    }


    publishPoint(data: any) {
        this.$point.next(data);
    }

}
