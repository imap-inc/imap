import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiConfig } from '../api.config';


@Injectable()
export class DialogService {
	Dialog: any;

	constructor(
	    private http: Http,
        private api: ApiConfig) {
		this.Dialog = {}
	}

	getAll(): Observable<any[]>{
		let url = this.api.url('/api/dialog?')  +  this.api.getTokenParams();
        return this.http.get(url).map(res => res.json())
			.catch(this.api.handleError);
	}



	getOneById(dialogId: string): Observable<any>{
		let url = this.api.url('/api/dialog/') + dialogId +  '?' + this.api.getTokenParams();
		return this.http.get(url).map(res => res.json())
			.catch(this.api.handleError);
	};



	getMessages(dialogId: string, from: number, count: number): Observable<any[]>{
		let url = this.api.url('/api/dialog/') + dialogId +  '?' + this.api.getTokenParams() + '&from=' + from + '&count=' + count;
		return this.http.get(url).map(res => res.json())
			.catch(this.api.handleError);
	}



	createDialog(userId: string): Observable<any>{
		return this.http.put(this.api.url('/api/dialog'), {'userId': userId}, this.api.getReqOption())
	  		.map(res => res.json())
			.catch(this.api.handleError);
	}
}
