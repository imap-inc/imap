import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiConfig } from '../api.config';

@Injectable()
export class PointService {

    constructor(
        private http: Http,
        private api: ApiConfig) {}

    getAll(): Observable<any[]>{
        let url = this.api.url('/api/point/all?') +  this.api.getTokenParams();
        return this.http.get(url).map(res => res.json())
        .catch(this.api.handleError);
    }

    getByUserId(userId: string): Observable<any[]>{
        let url = `${this.api.url('/api/point/by-user-id?')}user=${userId}&${this.api.getTokenParams()}`;
        return this.http.get(url).map(res => res.json())
        .catch(this.api.handleError);
    }

    toggleLike(pointId: string): Observable<any[]>{
        return this.http.post(this.api.url('/api/point/toggle-like'), {'id': pointId}, this.api.getReqOption())
        .map(res => res.json())
        .catch(this.api.handleError);
    }
}
