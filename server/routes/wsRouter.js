const router = require('express').Router();

let wsCtrl = require('controllers/wsCtrl');


router.ws('/dialog/:id/:token', wsCtrl.dialog);
router.ws('/:id/:token', wsCtrl.allInformation);


module.exports = router;
