const crypto = require('crypto');
const router = require('express').Router();
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const easyimg = require('easyimage');
const jimp = require("jimp");
const gm = require('gm').subClass({ imageMagick: true });

const apiCtrl = require('controllers/apiCtrl');



let uploadAvatar = multer({
	storage: multer.diskStorage({
		destination:(req, file, callback) => {
            let path = `../storage/public/${req.userId}/`;
            callback(null, path);
	    },
		filename: (req, file, cb) => {
			cb(null, 'avatar.jpg');
		}
	})
});



let uploadMsgImg = multer({
    storage: multer.diskStorage({
        destination:(req, file, callback) => {

            crypto.pseudoRandomBytes(16, function (err, raw) {
                req.name = raw.toString('hex') + Date.now() + path.extname(file.originalname);
                let dir = `../storage/dialogs/${req.params.id}`;
                easyimg.rescrop({
                    src: `${dir}/original/${req.name}`,
                    dst: `${dir}/250/${req.name}`,
                    width: 250,
                });

                callback(null, `../storage/dialogs/${req.params.id}/original`);
            });
        },
        filename: (req, file, cb) => {
            cb(null, req.name);
        }
    })
});






let uploadPointImg = multer({
    storage: multer.diskStorage({
        destination:(req, file, callback) => {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                req.name = raw.toString('hex') + Date.now() + path.extname(file.originalname);
                callback(null, `../storage/public/${req.userId}/points/original`);
            });
        },
        filename: (req, file, cb) => {
            cb(null, req.name);
        }
    })
});





router.get('/user', apiCtrl.user.getCurrent);
router.get('/user/all', apiCtrl.user.getAll);
router.get('/user/all-not-following', apiCtrl.user.getAllNotFollowing);
router.get('/user/all-following', apiCtrl.user.getAllFollowing);
router.get('/user/all-following-online', apiCtrl.user.getAllFollowingOnline);
router.post('/user/filter-by-id', apiCtrl.user.filterById);
router.post('/user/follow', apiCtrl.user.follow);
router.post('/user/unfollow', apiCtrl.user.unfollow);
router.post('/user/upload-avatar', uploadAvatar.single('file'), apiCtrl.user.uploadAvatar);

router.get('/user/:id', apiCtrl.user.getById);

router.get('/dialog/:id*', apiCtrl.dialog.getById);
router.post('/dialog/:id/upload-img', uploadMsgImg.single('file'), apiCtrl.dialog.uploadImg);
router.get('/dialog', apiCtrl.dialog.get);
router.put('/dialog', apiCtrl.dialog.create);

router.get('/point/all', apiCtrl.point.getAll);
router.get('/point/by-user-id', apiCtrl.point.getByUserId);
router.post('/point/add', uploadPointImg.single('file'), apiCtrl.point.create);
router.post('/point/toggle-like', apiCtrl.point.toggleLike);


router.get('/message', apiCtrl.message.get);



module.exports = router;
