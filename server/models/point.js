const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseUniqueValidator = require('mongoose-unique-validator');

const schema = new Schema({
    userId: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    userName: {type: String, required: true},
    date: { type: Date },
    fileName: {type: String, required: true},
    text: {type: String, required: true},
    coords: {
    	lat: {type: String},
    	lng: {type: String}
    },
    likes: [{type: Schema.Types.ObjectId, ref: 'User', unique: true, sparse: true}],
    comments: [{type: String}]
});

schema.pre('save', function(next) {
    let point = this;
    point.date = new Date();
    next();
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Point', schema);
