const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseUniqueValidator = require('mongoose-unique-validator');

const bcrypt = require('bcrypt-nodejs');
const SALT_WORK_FACTOR = 10;

const schema = new Schema({
	firstName: {type: String, required: true},
	lastName: {type: String, required: true},
	password: {type: String, required: true},
	email: {type: String, required: true, unique: true},
    avatar: {type: Boolean, default: false},
    isOnline: {type: Boolean, default: false},
    coords: {type: {
        lat: {type: String},
        lng: {type: String}
    }},
	dialogs: [{type: Schema.Types.ObjectId, ref: 'Dialog'}],
    followers: [{type: Schema.Types.ObjectId, ref: 'User'}],
    following: [{type: Schema.Types.ObjectId, ref: 'User'}],
    points: [{type: Schema.Types.ObjectId, ref: 'Point'}]
});



schema.pre('save', function(next) {
    let user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});



schema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('User', schema);
