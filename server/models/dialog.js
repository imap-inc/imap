const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String },
    date: { type: Date, required: true },
    countMessage: { type: Number, default: 0 },
    lastSender: { type: Schema.Types.ObjectId, ref: 'User' },
    twoMembers: { type: Object },
    lastMessage: {
        type: { type: String },
        content: { type: String },
    },
	users: [{
		_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
		unreadMsg: { type: Number, default: 0 },
	}],
	messages: [{
		msg: {
            type: { type: String, default: 'msg' },
            content: { type: String, required: true },
        },
		date: { type: Date, required: true },
		userName: { type: String },
		userId: { type: Schema.Types.ObjectId, ref: 'User', required:true }
	}]
});

module.exports = mongoose.model('Dialog', schema);
