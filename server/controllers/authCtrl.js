const fs = require('fs');
const jwt  = require('jwt-simple');

const User = require('models/user');

const config = require('config/main');





exports.signup = function(req, res) {
	if (!req.body.email || !req.body.password) {
		res.status(400).json({success: false, msg: 'Please pass name and password.'});
	} else {
		let newUser = new User({
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email,
			password: req.body.password
		});
		newUser.save(function(err, user) {
			if (err) {
				return res.status(400).json({success: false, msg: 'Username already exists.'});
			} else {
				fs.mkdir('../storage/public/' + user._id);
                fs.mkdir('../storage/public/' + user._id + '/points');
                fs.mkdir('../storage/public/' + user._id + '/points/original');
                fs.mkdir('../storage/public/' + user._id + '/points/600');
				res.json({success: true, msg: 'Successful created new user.'});
			}
		});
	}
};



exports.login = function(req, res) {
    User.findOne({
		email: req.body.email
	}, function(err, user) {
		if (err) throw err;
		if (!user) {
			res.status(400).send({success: false, msg: 'Authentication failed. User not found.'});
		} else {
			user.comparePassword(req.body.password, function (err, isMatch) {
				if (isMatch && !err) {
					let token = jwt.encode(user._id, config.secret);
		          	res.json({success: true, token: token});
				} else {
					res.status(400).send({success: false, msg: 'Authentication failed. Wrong password.'});
				}
			});
		}
	})
};



exports.logout = function(req, res){
	res.json({});
};
