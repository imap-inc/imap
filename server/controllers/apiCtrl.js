const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const easyimg = require('easyimage');
const jimp = require("jimp");
const gm = require('gm').subClass({ imageMagick: true });
const jwt  = require('jwt-simple');

const config = require('config/main');

const User = require('models/user.js');
const Dialog = require('models/dialog');
const Point = require('models/point');





exports.user = {
	getCurrent: function(req, res){
		User.findById(req.userId)
        .select("-password")
        .populate({ path: 'points', options: { sort: '-date'}})
        .exec( function(err, user){
            if(err) res.status(500).json(err);
            else res.status(200).json(user);
        });
	},

	getById: function(req, res){
		User.findById(req.params.id)
        .select("-password")
        .populate({ path: 'points', options: { sort: '-date'}})
        .exec( function(err, user){
            if(err) res.status(500).json(err);
            else res.status(200).json(user);
        });
	},

    filterById: function(req, res){
        User.find({'_id': {$in: req.body.arrayIds, $nin: req.userId}})
            .select("coords firstName lastName avatar isOnline")
            .exec( function(err, user){
                if(err) res.status(500).json(err);
                else res.status(200).json(user);
            });
    },

	getAll: function(req, res){
		User.find({}, function(err, user){
			if(err) res.status(500).json(err);
			else res.status(200).json(user);
		}).select("-password");
	},

	getAllNotFollowing: function(req, res){
		User.findById(req.userId, function(err, user){
			let excludeUsers = user.following;
			excludeUsers.push(user._id);

            let params = {
                '_id': {$nin: excludeUsers}
            };

            if(req.query.name){
                params['$and'] = [
                    {'firstName': {'$regex' : req.query.name.split(' ')[0],  '$options' : 'i'}},
                    {'lastName': {'$regex' : req.query.name.split(' ')[1] || '',  '$options' : 'i'}}
                ];
            }

			User.find(params, function(err, user){
				if(err) res.status(500).json(err);
				else res.status(200).json(user);
			}).select("_id firstName lastName avatar isOnline coords");
		});
	},

	getAllFollowing: function(req, res){
		User.findById(req.userId, function(err, user){

            let params = {
                '_id': {$in: user.following}
            };

            if(req.query.name){
                params['$and'] = [
                    {'firstName': {'$regex' : req.query.name.split(' ')[0],  '$options' : 'i'}},
                    {'lastName': {'$regex' : req.query.name.split(' ')[1] || '',  '$options' : 'i'}}
                ];
            }

			User.find(params, function(err, user){
				if(err) res.status(500).json(err);
				else res.status(200).json(user);
			}).select("_id firstName lastName avatar isOnline coords");
		});
	},

	getAllFollowingOnline: function(req, res){
		User.findById(req.userId, function(err, user){

            let params = {
                '_id': {$in: user.following},
                'isOnline': true
            };

            if(req.query.name){
                params['$and'] = [
                    {'firstName': {'$regex' : req.query.name.split(' ')[0],  '$options' : 'i'}},
                    {'lastName': {'$regex' : req.query.name.split(' ')[1] || '',  '$options' : 'i'}}
                ];
            }

			User.find(params, function(err, user){
				if(err) res.status(500).json(err);
				else res.status(200).json(user);
			}).select("_id firstName lastName avatar isOnline coords");
		});
	},

	follow: function(req, res){
		User.findById(req.userId, function(err, user){
			if(user.following.indexOf(req.body.id) == -1){
				user.following.push(req.body.id);
				user.save();

				User.findById(req.body.id, function(err, user2){
					user2.followers.push(req.userId);
					user2.save();
				});
			}
			res.status(200).json({'userId': req.body.id});
		});
	},

	unfollow: function(req, res){
		User.findById(req.userId, function(err, user){
			let index = user.following.indexOf(req.body.id);
			if (index != -1) {
			    user.following.splice(index, 1);
				user.save();

				User.findById(req.body.id, function(err, user2){
					index = user2.followers.indexOf(req.userId);
					user2.followers.splice(index, 1);
					user2.save();
				});
			}
			res.status(200).json({'userId': req.body.id});
		});
	},

	uploadAvatar: function(req, res){
        let callbackCount = 0;
        let callbackMustBe = 4;

        function checkCallback(number){
            // callbackCount ++;
            // console.log(`--- ${number} ---`);
            // if(callbackCount == callbackMustBe){
            //     console.log(`--- done ---`);
            // }
        }

        let id = req.userId;
        let path = `../storage/public/${id}/`;
        let src = path + "avatar.jpg";

        easyimg.rescrop({
            src: src, dst: path + 'avatar.jpg',
            width: 250,
            fill: true
        }).then(checkCallback(1));

        easyimg.rescrop({
            src: src, dst: path + 'avatar-80.jpg',
            width: 80,
            fill: true
        }).then(checkCallback(2));

        easyimg.rescrop({
            src: src, dst: path + 'avatar-30.jpg',
            width: 30,
            fill: true
        }).then(checkCallback(3));

        easyimg.rescrop({
            src: src, dst: path + 'avatar.png',
            format: 'png',
            width: 44,
            fill: true
        }).then(() => {
            let outputPath = path + 'avatar.png';
            let size = 44;

            gm(outputPath)
                .write(outputPath, function() {
                    gm(size, size, 'none')
                        .fill(outputPath)
                        .drawCircle(size/2,size/2, size/2, 0)
                        .write(outputPath, function(err) {
                            jimp.read(path + 'avatar.png', function (err, img) {
                                if (err) throw err;
                                jimp.read('../storage/public/marker.png', function (err, img2) {
                                    if (err) throw err;
                                    img2.composite(img, 5, 5).write(path + 'marker.png');
                                    checkCallback(4);
                                });
                            });
                        });
                });

        });

		User.findById(req.userId, function(err, user){
			user.avatar = true;
			user.save();
		});
		res.status(200).json({
      		originalName: req.file.originalname,
      		filename: req.file.filename
	    });
	}
};






exports.dialog = {
	get: function(req, res){
		User.findById(req.userId, function(err, user){
			if(user.dialogs.length != 0){
				Dialog.find({ '_id': { $in: user.dialogs } }, function(err, rows){
					res.status(200).json(rows);
				}).select('-messages');
			} else {
				res.status(200).json([]);
			}
		});
	},



    uploadImg: function(req, res){
        let counter = 0;
        let interval = setInterval(() => {
            counter++;
            if (fs.existsSync(`../storage/dialogs/${req.params['id']}/250/${req.file.filename}`)) {
                res.status(200).json({
                    filename: req.file.filename
                });
                clearInterval(interval);
            } else if(counter == 10){
                clearInterval(interval);
                res.status(400);
            }
        },100);
    },



	getById: function(req, res){
		let slice = -50;
		let countMessage = -1;
		let onlyMessages = false;

		User.find({'_id': req.userId, 'dialogs': {'$in': [req.params.id]}}, function(err, user){
			if(err || user.length == 0 ) res.status(403).json({error: 'Access denide'});

			if(req.query.from && req.query.count){
				onlyMessages = true;
				countMessage = parseInt(req.query.from) + parseInt(req.query.count);
				slice = [-1*countMessage, parseInt(req.query.from)];
			}

			Dialog.findById(req.params.id, { 'messages': { $slice: slice } })
				.exec( function(err, dialog){
					if(err){

						res.status(406).json(err);

					} else {
						let curentUser = dialog.users.find((user, index) => {
							if(user._id == req.userId){
								dialog.users[index].unreadMsg = 0;
							}
						});

						dialog.save();

						if(dialog.countMessage > countMessage){

							res.status(200).json(onlyMessages ? dialog.messages : dialog);

						} else if(dialog.countMessage < countMessage && dialog.countMessage > req.query.from){

							res.status(200).json(dialog.messages.slice(-1*req.query.from, (dialog.countMessage - req.query.from)).reverse());

						} else res.status(200).json({'theEnd': true});
					}
				});

		});


	},



	create: function(req, res){
		User.findById(req.body.userId, function (err, user) {
			Dialog.find({'$and': [
					{'users': {'$elemMatch': {'_id': mongoose.Types.ObjectId(req.userId)} } },
					{'users': {'$elemMatch': {'_id': mongoose.Types.ObjectId(req.body.userId)} } } ]
				}, function(err, dialogs) {
				if(dialogs && dialogs.length != 0){
			        res.status(200).json(dialogs[0]);
                } else {
					let dialog = new Dialog({
						lastMessage: {},
						lastSender: req.userId,
						name: '',
						date: new Date(),
						users: [{'_id': req.userId, 'unreadMsg': 0}, {'_id': req.body.userId, 'unreadMsg': 0}],
						twoMembers: {}
					});
					dialog.twoMembers = {};
					dialog.twoMembers[req.userId] = user.firstName + ' ' + user.lastName;

					user.dialogs.push(dialog._id);
					user.save();

					User.findById(req.userId, function (err, user) {
						user.dialogs.push(dialog._id);
						user.save();

						dialog.twoMembers[req.body.userId] = user.firstName + ' ' + user.lastName;

						dialog.save(() => {
                            fs.mkdir(`../storage/dialogs/${dialog._id}`);
                            fs.mkdir(`../storage/dialogs/${dialog._id}/original`);
                            fs.mkdir(`../storage/dialogs/${dialog._id}/250`);
                            res.status(200).json(dialog);
						});
					});
				}
			})

		});
	}
};






exports.point = {

    getAll: function(req, res){
        Point.find({})
        .sort({date: -1})
        .exec( function(err, user){
            if(err) res.status(500).json(err);
            else res.status(200).json(user);
        });
    },

    getByUserId: function(req, res){
        Point.find({userId: req.body.id}, function(err, data){
            if(err) res.status(500).json(err);
            else res.status(200).json(data);
        });
    },

    toggleLike: function(req, res){
        Point.findById(req.body.id, function(err, point){
            if(err) res.status(500).json(err);
            else{
                let index = point.likes.indexOf(req.userId);

                if(index == -1){
                    point.likes.push(req.userId)
                } else {
                    point.likes.splice(index, 1);
                }

                point.save();

                res.status(200).json(point);
            }
        });
    },

    create: function(req, res) {
        let dir = `../storage/public/${req.userId}/points`;

        easyimg.rescrop({
            src: `${dir}/original/${req.name}`,
            dst: `${dir}/600/${req.name}`,
            width: 600,
            height: 400,
            fill: true,
        });

        User.findById(req.userId, function(err, user){
            if(err){
                res.status(500).json(err);
            } else{
                let point  = new Point({
                    userId: req.userId,
                    userName: `${user.firstName} ${user.lastName}`,
                    fileName: req.file.filename,
                    text: req.body.text,
                    coords: {
                        lat: req.body.lat,
                        lng: req.body.lng,
                    }
                });

                point.save((err, data)=>{
                    console.log(err);
                    user.points.push(point._id);
                    user.save();
                    res.status(200).json(point);
                });
            }
        });
    },
};






exports.message = {
	get: function(req, res) {
		Dialog.findById(req.query.id , function(err, dialog){
			res.status(200).json(dialog);
		});
	},
};
