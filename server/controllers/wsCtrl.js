const mongoose = require('mongoose');
const async = require('async');

const User = require('models/user');
const Dialog = require('models/dialog');

const config = require('config/main');
const jwt  = require('jwt-simple');
const request = require('sync-request');


const dialogsWs = {};
const users = {};

formatingCoords = (coords) => {
    coords = coords.split(' ').join(',');
    return coords;
};

exports.dialog = (ws, req) => {
	const userId = jwt.decode(req.params.token, config.secret);

	if(!dialogsWs[req.params.id]) dialogsWs[req.params.id] = {};
	dialogsWs[req.params.id][userId] = ws;

	ws.on('message', function(msg) {
		User.findById(userId, function (err, user) {
		    msg = JSON.parse(msg);
			if(msg.content[0] == '"' && msg.content[msg.content.length-1] == '"'){
				msg.content = msg.content.replace('"', '');
				msg.content = msg.content.replace('"', '');
			}

			let message = {
		    	userId: userId,
		    	date: new Date(),
		    	userName: user.firstName + ' ' + user.lastName
		    };

			if(msg.type == 'coords'){
			    let url = `http://maps.googleapis.com/maps/api/geocode/json?latlng=${formatingCoords(msg.content)}&sensor=true`;
                let res = request('GET', url);
                msg.content = JSON.stringify({
                    'formatted_address': JSON.parse(res.getBody())['results'][0].formatted_address,
                    'coords': msg.content
                });
                message.msg = msg
            } else {
                message.msg = msg;
            }

	    	Dialog.findById(req.params.id, function(err, dialog){
	    		dialog.messages.push(message);
	    		dialog.lastMessage = {
	    		    type: msg.type,
                    content: msg.content
                };
	    		dialog.lastSender = message.userId;
	    		dialog.date = message.date;
	    		dialog.countMessage = dialog.countMessage + 1;

	    		let messageSTR = JSON.stringify(message);

                async.forEach(dialogsWs[req.params.id], function (item){
                    item.send(messageSTR);
                });

    			let tmpDialog = dialog;
    			delete tmpDialog['messages'];

    			let tmpDialogSTR = JSON.stringify({ type: 'dialog', data: tmpDialog });

                async.forEach(dialog.users, function (item){
                    if(!dialogsWs[req.params.id][item._id]){
                        item.unreadMsg += 1;
                    }
                    if(users[item._id]){
                        users[item._id].send(tmpDialogSTR);
                    }
                });

	    		dialog.save();
	    	});
		});
	});

	ws.on('close', function () {
   		delete dialogsWs[req.params.id][userId];
   		if(Object.keys(dialogsWs[req.params.id]).length == 0){
   			delete dialogsWs[req.params.id];
   		}
	});
};



exports.allInformation = function(ws, req) {
	const userId = jwt.decode(req.params.token, config.secret);

	users[userId] = ws;

	User.findById(userId, function (err, user) {
		user.isOnline = true;
		user.save();
	});

	ws.on('message', function(obj) {
		obj = JSON.parse(obj);
		if(obj.type == 'dialog'){

		} else if(obj.type == 'coords'){
			User.findById(userId, function (err, user) {
				user.coords = {};
				user.coords.lat = obj.data.lat;
				user.coords.lng = obj.data.lng;
				user.save();
			});
		}

	});

	ws.on('close', function () {
   		delete users[userId];

   		User.findById(userId, function (err, user) {
			user.isOnline = false;
			user.save();
		});
	});
};
