const jwt  = require('jwt-simple');
const config = require('config/main');

const apiMiddleware = function (req, res, next) {
	let token = req.headers['x-access-token'] || req.query._token || req.headers['authorization'];
	if(token != 'null' && token){
		req.userId = jwt.decode(token, config.secret);
		next();
	} else {
		res.status(401).json({"error": "Access denied"});
	}
};

module.exports = apiMiddleware;
