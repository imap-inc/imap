// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const https = require('https');
const fs = require('fs');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');

const config = require('./server/config/main');
const WORK = process.env.NODE_ENV || 'dev';


const app = express();
const app_http = express();
const options = {
    ca: fs.readFileSync('imap_live.ca-bundle', 'utf8'),
    key  : fs.readFileSync('server.key', 'utf8'),
    cert : fs.readFileSync('imap_live.crt', 'utf8')
};
const server = https.createServer(options, app);
const httpServer = http.createServer(app_http);
const expressWs = require('express-ws')(app, server);
const db = mongoose.connect(config.database[WORK]);
const port = process.env.PORT || config.port[WORK]; // 443 for production

require('app-module-path').addPath(__dirname + '/server');


// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// app.use('*/avatar.jpg', function (req, res, next) {
//     fs.stat(`../storage/public/${req.originalUrl}`, function(err, stat) {
//         if(err == null) {
//         } else if(err.code == 'ENOENT') {
//             if(req.params.id){
//                 console.log(req.params.id);
//                 res.sendFile(path.join(__dirname, '../storage/public/no-avatar.png'));
//             }
//             // return res.sendfile(fs.stat('../storage'))
//         } else {
//             console.log('Some other error: ', err.code);
//         }
//     });
//     next();
// });

app.use(express.static(path.join(__dirname, '../storage/public')));
app.use(express.static(path.join(__dirname, '../storage/dialogs')));


// For WebSocket


// Parsers for POST data
app.use(bodyParser.urlencoded( {extended: true }));
app.use(bodyParser.json({uploadDir:'../storage'}));


app.use(cookieParser());


app.use('/available-api', require('./server/routes/availableApiRouter'));


// Set our api routes middleware
app.use('/api', require('./server/middleware/apiMiddleware'));


// Set our api routes
app.use('/api', require('./server/routes/apiRouter'));
app.use('/ws', require('./server/routes/wsRouter'));


app.use('/auth', require('./server/routes/authRouter'));

// Catch all other routes and return the index file
app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
app.set('port', port);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));




app_http.set('port', port);
app_http.get('*', (req, res) => {
    res.redirect(`https://${req.headers['host']}`);
});
httpServer.listen('80');
